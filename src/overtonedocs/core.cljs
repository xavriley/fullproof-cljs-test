(ns overtonedocs.core
  (:require [clojure.browser.repl :as repl]
            [domina :as dom]
            [domina.events :as ev]
            [fullproof :as fullproof]))

(def marioData
  (clj->js
       [{:name "Mario", :type "Protagonist"},
        {:name "Luigi", :type "Protagonist"},
        {:name "Princess Peach", :type "Protagonist"},
        {:name "Toad", :type "Protagonist"},
        {:name "Yoshi", :type "Protagonist"},
        {:name "Toadsworth", :type "Supporting"},
        {:name "Donkey Kong", :type "Supporting"},
        {:name "Princess Daisy", :type "Supporting"},
        {:name "Professor E.Gadd", :type "Supporting"},
        {:name "Baby Mario", :type "Supporting"},
        {:name "Rosalina", :type "Supporting"},
        {:name "Pauline", :type "Supporting"},
        {:name "Birdo", :type "Supporting"},
        {:name "Toadette", :type "Supporting"},
        {:name "Bowser", :type "Antagonist"},
        {:name "Bowser Jr", :type "Antagonist"},
        {:name "Fawful", :type "Antagonist"},
        {:name "Kammy Koopa", :type "Antagonist"},
        {:name "Kamek", :type "Antagonist"},
        {:name "King Boo", :type "Antagonist"},
        {:name "Petey Piranha", :type "Antagonist"},
        {:name "Wario", :type "Antagonist"},
        {:name "Waluigi", :type "Antagonist"},
        {:name "Wart", :type "Antagonist"},
        {:name "Koopa Kid", :type "Antagonist"},
        {:name "Tatanga", :type "Antagonist"}]))

(def ^:export dbName (str "mario")) ;; single quoted for light table bug
(def ^:export marioSearchEngine (.BooleanEngine js/fullproof))
(def ^:export capabilities (.setDbSize
                           (.setDbName
                           (.setUseScores
                            (.Capabilities js/fullproof) false) (str dbName)) (* 1 1024 1024)))

(defn ^:export loadSearchIndex [injector callback]
  (let [synchro (fullproof/make_synchro_point callback (count marioData))]
    (doseq [[idx item] (map-indexed (fn [itm idx] [itm idx]) marioData)]
      (injector/inject (str (.-name item) " " (.-type item))
                       idx
                       synchro))))

(defn ^:export cssShow [el]
  (dom/set-style! el :display "block"))

(defn ^:export cssHide [el]
  (dom/set-style! el :display "hidden"))

(defn ^:export engineReady [callback]
  (cond callback
        (do
          (cssShow (dom/by-id "application")) 
          (cssHide (dom/by-id "loading")))
        (js/alert) "Can't open the search engine"))

(def ^:export index1
  (clj->js {:name "normalindex"
           :analyzer (.StandardAnalyzer js/fullproof (.-to_lowercase_nomark (.-normalizer js/fullproof)))
           :capabilities capabilities
           :index marioData
           :initializer loadSearchIndex}))

(defn ^:export logResults [x]
  (js/console.log x))

(defn search []
  (let [searchbox (dom/by-id "typehere")
        results   (dom/by-id "results")]
      (.lookup marioSearchEngine (.-value searchbox)
              (fn [result-set] 
                (logResults result-set)
                (doseq [result-idx (.-data result-set)]
                  (let [result (nth marioData result-idx)]
                    (logResults (.-name result))
                    (logResults (.-type result))
                    (dom/append! (dom/by-id "results")
                                        (str "<p>" (.-name result) " " (.-type result) "</p>"))))))))

(defn ^:export initialise []
  (repl/connect "http://localhost:9000/repl")
  ;; for some reason, index and initializer are undefined
  ;; in marioSearchEngine.indexes[0]
  (.open marioSearchEngine (js/Array. index1) (fullproof/make_callback engineReady true) (fullproof/make_callback engineReady true))
  ;;(js/openSearchEngine)
  (ev/listen! (dom/by-id "search") :click search)
  (ev/listen! (dom/by-id "typehere") :change search))

(set! (.-onload js/window) initialise)
